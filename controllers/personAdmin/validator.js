const {check} = require('express-validator')


exports.add = [check('name','กรุณาป้อนข้อมูลชื่อ-นามสกุล').not().isEmpty(),check('studentcode','กรุณาป้อนรหัสนักศึกษา').not().isEmpty(),check('mobile','กรุณาป้อนเบอร์โทรติดต่อ').not().isEmpty(),check('major_id','กรุณาเลือกโปรแกรมวิชา').isNumeric(),check('group_id','กรุณาเลือกชั้นปี').isNumeric()];
exports.update = [check('name','กรุณาป้อนข้อมูลชื่อ-นามสกุล').not().isEmpty(),check('studentcode','กรุณาป้อนรหัสนักศึกษา').not().isEmpty(),check('mobile','กรุณาป้อนเบอร์โทรติดต่อ').not().isEmpty(),check('major_id','กรุณาเลือกโปรแกรมวิชา').isNumeric(),check('group_id','กรุณาเลือกชั้นปี').isNumeric()];
