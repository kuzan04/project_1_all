const controller = {}
const {
  validationResult
} = require('express-validator')

controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    req.getConnection((err, conn) => {
      conn.query('select p.id,p.name as pname,p.studentcode as pstu,p.mobile as pm,m.name as mname,dp.name as dname,g.name as gname  from person as p JOIN major as m on m.id = p.major_id JOIN _group as g on g.id=p.group_id JOIN department as dp on m.department_id=dp.id ;', (err, prs) => {
        if (err) {
          res.json(err);
        }
        res.render('./personAdmin/personAdminList', {
          data: prs, session:req.session
        });
      });
    });
  }
};
controller.new = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const data = null;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM major', (err, major) => {
        conn.query('SELECT * FROM _group', (err, _group) => {
          res.render('./personAdmin/personAdminAdd', {
            data1: major,
            data2: _group,
            data3: data,
            session: req.session
          });
        });
      });
    });
  }
};
controller.save = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/personAdmin/new')
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
    const data = req.body;
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO person set ?', [data], (err, person) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/personAdmin');
      });
    });
  }
  }
};
controller.delete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
      res.redirect('/');
  } else {
      const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
        const {
          id
        } = req.params;
        const data=req.body;
    req.getConnection((err, conn) => {
      conn.query('delete from person where id=?', [id], (err, device) => {
            if (err) {
                const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
                req.session.errors = errorss;
                req.session.success = false;
            } else {
                req.session.success = true;
                req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        }
        res.redirect('/personAdmin');
      });
    });
  }
};


controller.checkdelete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department', (err, department) => {
        conn.query('SELECT * FROM _group', (err, _group) => {
          conn.query('SELECT * FROM major', (err, major) => {
            conn.query('SELECT * FROM person WHERE id = ?',[id], (err, person) => {
              res.render('./personAdmin/personAdminDelete', {
                data3: department,
                data2: _group,
                data1: major,
                data: person,
                session: req.session
              });
            });
          });
        });
    });
  });
  }
};

controller.edit = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM _group ', (err, group) => {
        conn.query('SELECT * FROM major', (err, major) => {
          conn.query('SELECT * FROM person WHERE id = ?', [id], (err, person) => {
            res.render('./personAdmin/personAdminUpdate', {
              data: group,
              data1: major,
              data2: person,
              session: req.session
            });
          });
        });
      });
    });
  }
};

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {    const errors = validationResult(req);
          if (!errors.isEmpty()) {
              req.session.errors = errors;
              req.session.success = false;
              res.redirect(req.get('referer'))
          } else {
              req.session.success = true;
              req.session.topic = "แก้ไขข้อมูลสำเร็จ";
    const { id } = req.params;
    const data = req.body;
    req.getConnection((err, conn) => {
      conn.query('update person set ? where id=?', [data, id], (err, person) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/personAdmin');
      });
    });
  }
  }
};


module.exports = controller;
