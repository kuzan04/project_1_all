const controller = {};

controller.list = (req,res) =>{
    if(typeof req.session.userid == 'undefined'){ res.redirect('/') }else{
        req.getConnection((err,conn)=>{
            conn.query('select p.name as pname,p.studentcode as studentcode,p.mobile as mobile,m.name as mname,dp.name as dpname,g.name as gname, d.name as dname,d.code as code, t.name as tname,ro.name as roname,DATE_FORMAT(b.borrow_date,"%Y/%m/%d") as bdate, DATE_FORMAT(b._return,"%Y/%m/%d") as breturn, DATE_FORMAT(r.get_date, "%Y/%m/%d") as rget from _return as r RIGHT JOIN borrow as b ON r.borrow_id = b.id JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id JOIN major as m ON p.major_id = m.id JOIN department as dp ON m.department_id = dp.id JOIN _group as g ON p.group_id = g.id JOIN type as t ON d.type_id = t.id JOIN room as ro ON d.room_id = ro.id;',(err,all)=>{
                if(err){
                    res.json(err);
                }else{
                    res.render('./mainAdmin/mainAdmin',{
                        data:all, session:req.session
                    });
                }
            })
        })
    }
}

module.exports = controller;