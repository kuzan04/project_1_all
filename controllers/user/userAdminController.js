const controller = {};
const {
    validationResult
} = require('express-validator');

controller.list = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT u.id,u.username, u.password, p.name as pname, p.mobile, p.studentcode, u.admin, m.name as mname, g.name as gname, dp.name as dpname from user as u JOIN person as p ON u.person_id = p.id JOIN major as m ON p.major_id = m.id JOIN _group as g ON p.group_id = g.id JOIN department as dp ON m.department_id = dp.id;', (err, userAdmin) => {
                if (err) {
                    res.json(err)
                }
                res.render('./user/user', {
                    data: userAdmin,
                    session: req.session,
                    data2: req.session.userid
                })
            })
        })
    }
}

controller.new = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = null;
        req.getConnection((err, conn) => {
            conn.query('select * from person', (err, person) => {
                conn.query('select * from user', (err, user) => {
                    res.render('./user/userAdd', {
                        data: person,
                        data1: user,
                        session: req.session
                    });
                    // res.send(person);
                })
            })
        });
    };
};

controller.save = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/user/new')
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
            //     // Database Add
            const data = req.body;
            req.getConnection((err, conn) => {
                conn.query('INSERT INTO user set ?', [data], (err, user) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/user');
                });
            });
            // 
        }
    };
};

controller.check = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const {
            id
        } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM person', (err, person) => {
                conn.query('SELECT * FROM user WHERE id = ?;', [id], (err, user) => {
                    if (err) {
                        res.json(err);
                    }
                    res.render('./user/userDel', {
                        data: person,
                        data1: user,
                        session: req.session
                    });
                })
            });
        });
    };
};

controller.delete = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/user')
        } else {
            req.session.success = true;
            req.session.topic = "ลบข้อมูลสำเร็จ";
            const {
                id
            } = req.params;
            req.getConnection((err, conn) => {
                conn.query('DELETE FROM user WHERE id=?', [id], (err, user) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/user');
                });
            });
        };
    };
}
controller.edit = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const {
            id
        } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM person', (err, person) => {
                conn.query('SELECT * FROM user WHERE id=?', [id], (err, user) => {
                    res.render('./user/userUpdate', {
                        data: person,
                        data1: user,
                        session: req.session
                    });
                });
            })
        });
    };
};

controller.update = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect(req.get('referer'))
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
            //     // Database Add
            const {
                id
            } = req.params;
            const data = req.body;
            req.getConnection((err, conn) => {
                conn.query('UPDATE user set ? WHERE id=?', [data, id], (err, user) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/user');
                });
            });
            // 
        }
    };
}
//};
module.exports = controller;