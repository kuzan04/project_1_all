const {check} = require('express-validator');

exports.Check = [check('person_id','กรุณาเลือกบุคคล').isNumeric(),check('username','กรุณาป้อนชื่อผู้ใช้งาน').not().isEmpty(),check('password','กรุณาป้อนรหัสผู้ใช้งาน').not().isEmpty(),check('admin','กรุณาเลือกตำแหน่ง').isNumeric()]
