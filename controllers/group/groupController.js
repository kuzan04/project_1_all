const controller = {};
const { validationResult } = require('express-validator');

controller.list = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT g.name AS gname, g.id FROM _group AS g ORDER BY g.id', (err, groupList) => {
                if (err) {
                    res.json(err);
                }
                res.render('./group/groupList', {
                    data: groupList, session: req.session
                });
            });
        });
    };
};

controller.new = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const data = null;
        req.getConnection((err, conn) => {
            res.render('./group/groupNew', {
                data1: data, session: req.session
            });
        });
    };
};

controller.add = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/group/new')
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
            //     // Database Add
            const data = req.body;
            req.getConnection((err, conn) => {
                conn.query('INSERT INTO _group set ?', [data], (err, groupAdd) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/group');
                });
            });
            // 
        }
    };
};

controller.del = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT g.name AS gname, g.id FROM _group AS g WHERE id=?;', [id], (err, groupDel) => {
                if (err) {
                    res.json(err);
                }
                res.render('./group/groupDel', {
                    data: groupDel, session: req.session
                });
            });
        })
    };
};

controller.confirm = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        // const errors = validationResult(req);
        const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] };

        //     // Database Add
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('DELETE FROM _group WHERE id=?', [id], (err, groupConfirm) => {
                if (err) {
                    req.session.errors = errorss;
                    req.session.success = false;
                } else {
                    req.session.success = true;
                    req.session.topic = "ลบข้อมูลสำเร็จ";
                }
                res.redirect('/group');
            });
        });
        // 
    };
};

controller.edit = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT * FROM _group AS g WHERE id=?', [id], (err, groupEdit) => {
                res.render('./group/groupEdit', {
                    data1: groupEdit, session: req.session
                });
            });
        });
    };
};

controller.update = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect(req.get('referer'))
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
            //     // Database Add
            const { id } = req.params;
            const data = req.body;
            req.getConnection((err, conn) => {
                conn.query('UPDATE _group set? WHERE id=?', [data, id], (err, groupUpdate) => {
                    if (err) {
                        res.json(err);
                    }
                    res.redirect('/group');
                });
            });
            // 
        }
    };
};

module.exports = controller;