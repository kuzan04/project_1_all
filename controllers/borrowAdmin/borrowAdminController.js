const controller = {};
const {
  validationResult
} = require('express-validator')

controller.edit = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM person', (err, person) => {
        conn.query('SELECT * FROM device', (err, device) => {
          conn.query('SELECT b.id, b.person_id, b.device_id, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as borrow_date, DATE_FORMAT(b._return, "%Y/%m/%d") as _return FROM borrow as b WHERE id = ?', [id], (err, borrow) => {
            res.render('./borrowAdmin/borrowAdminUpdate', {
              data1: person,
              data2: device,
              data3: borrow,
              session: req.session
            });
            // res.send(borrow)
          });
        });
      });
    });
  };
}

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect(req.get('referer'))
        } else {
            req.session.success = true;
            req.session.topic = "แก้ไขข้อมูลสำเร็จ";
    const { id  } = req.params;
    const data = req.body;
    req.getConnection((err, conn) => {
      conn.query('UPDATE borrow SET ? WHERE id=?', [data, id], (err, borrow) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/borrowAdmin');

      });
    });
  };
}
}

controller.confirm = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const { id } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM person', (err, person) => {
        conn.query('SELECT * FROM device', (err, device) => {
          conn.query('SELECT b.id, b.person_id, b.device_id, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as borrow_date, DATE_FORMAT(b._return, "%Y/%m/%d") as _return FROM borrow as b WHERE id = ?', [id], (err, borrow) => {
            res.render('./borrowAdmin/borrowAdminCheck', {
              data1: person,
              data2: device,
              data3: borrow,
              session: req.session
            });
          });
        });
      });
    });
  };
}
// controller.save = (req, res) => {
//   if (typeof req.session.userid == 'undefined') {
//     res.redirect('/');
//   } else {
//     const data = req.body;
//     req.getConnection((err, conn) => {
//       conn.query('INSERT INTO borrow set ?', [data], (err, borrow) => {
//         if (err) {
//           res.json(err);
//         }
//         res.redirect('/borrowAdmin');
//       });
//     });
//   };
// }
// }
controller.save = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
        if (!errors.isEmpty()) {
            req.session.errors = errors;
            req.session.success = false;
            res.redirect('/borrowAdmin/new')
        } else {
            req.session.success = true;
            req.session.topic = "เพิ่มข้อมูลสำเร็จ";
    const data = req.body;
    req.getConnection((err, conn) => {
      conn.query('INSERT INTO borrow SET ?', [data], (err, borrow) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/borrowAdmin')
      })
    })
  }
}
}

controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    req.getConnection((err, conn) => {
      conn.query('select b.id, p.name as pname, DATE_FORMAT(b.borrow_date,"%Y/%m/%d") as date, DATE_FORMAT(b._return,"%Y/%m/%d") as gd,d.name as dename from borrow as b join person as p on b.person_id = p.id join device as d on b.device_id = d.id  order by b.id', (err, borrow) => {
        if (err) {
          res.json(err);
        }
        res.render('./borrowAdmin/borrowAdmin', {
          data: borrow,
          session: req.session
        });
      });
    });
  };
}

controller.delete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
      res.redirect('/');
  } else {
      const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
        const {
          id
        } = req.params;
        const data=req.body;
    req.getConnection((err, conn) => {
      conn.query('delete from borrow where id=?', [id], (err, borrow) => {
            if (err) {
                const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
                req.session.errors = errorss;
                req.session.success = false;
            } else {
                req.session.success = true;
                req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        }
        res.redirect('/borrowAdmin');
      });
    });
  }
};
//  if (typeof req.session.userid == 'undefined') {
//    res.redirect('/');
//  } else {
//        const errors = validationResult(req);
  //      if (!errors.isEmpty()) {
//            req.session.errors = errors;
//            req.session.success = false;
//            res.redirect(req.get('referer'))
//        } else {
//            req.session.success = true;
//            req.session.topic = "ลบข้อมูลสำเร็จ";
//    const {
//      id
//    } = req.params;
//    req.getConnection((err, conn) => {
//      conn.query('delete from borrow where id=?', [id], (err, borrow) => {
//        if (err) {
//          res.json(err);
//        }
//        res.redirect('/borrowAdmin');
//      });
//    });
//  });

//}
//}


controller.new = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    // const data = null
    var date = new Date();
    var today = date.getFullYear() + '/' + date.getMonth() + '/' + date.getDate();
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM person', (err, person) => {
        conn.query('SELECT * FROM device', (err, device) => {
          conn.query('SELECT * FROM borrow', (err, borrow) => {
            conn.query('SELECT DATE_FORMAT(CURDATE(),"%Y/%m/%d") as CUR', (err, now) => {
              res.render('./borrowAdmin/borrowAdminAdd', {
                data1: person,
                data2: device,
                data3: borrow,
                data4: now,
                session: req.session
              })
              // res.send(now);
            });
          });
        });
      });
    });
  };
}

controller.detail = (req,res) =>{
    if(typeof req.session.userid == 'undefined'){
        res.redirect('/')
    }else{
        const {id} = req.params;
        req.getConnection((err,conn)=>{
            conn.query('select b.id as bid, p.name as pname, p.studentcode as studentcode, p.mobile as mobile, m.name as mname, dp.name as dpname, g.name as gname, d.name as dname, d.code as code, t.name as tname, ro.name as roname, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as bdate, DATE_FORMAT(b._return, "%Y/%m/%d") as breturn from borrow as b JOIN person as p ON b.person_id = p.id JOIN major as m ON p.major_id = m.id JOIN department as dp ON m.department_id = dp.id JOIN _group as g ON p.group_id = g.id JOIN device as d ON b.device_id = d.id JOIN type as t ON d.type_id = t.id JOIN room as ro ON d.room_id = ro.id HAVING bid =?',[id],(err,borrows)=>{
                if(err){
                    res.json(err)
                }
                res.render('./borrowAdmin/borrowAdminDetail',{
                    data:borrows, session:req.session, data1:req.session.userid
                })
            })
        })
    }
}


module.exports = controller;
