const {check} = require('express-validator');

exports.Check = [check('person_id','กรุณาเลือกบุคคลที่ต้องการยืม').isNumeric(),check('device_id','กรุณาเลือกอุปกรณ์ที่ต้องการยืม').isNumeric(),check('borrow_date','กรุณาระบุวันที่ยืม').not().isEmpty(),check('_return','กรุณาระบุวันคืน').not().isEmpty()];
