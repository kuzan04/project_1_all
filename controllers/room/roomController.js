const controller = {};
const { validationResult } = require("express-validator");

controller.list = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM room", (err, room) => {
        if (err) {
          res.json(err);
        }
        res.render("./room/roomAdminList", {
          data: room,
          session: req.session,
        });
      });
    });
  }
};

controller.new = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const data = null;
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM room", (err, room) => {
        res.render("./room/roomAdminAdd", {
          data: room,
          session: req.session,
        });
      });
    });
  }
};

// controller.add = (req, res) => {
//   if (typeof req.session.userid == 'undefined') {
//     res.redirect('/');
//   } else {
//     const data = req.body;
//     req.getConnection((err, conn) => {
//       conn.query('INSERT INTO room set ?', [data], (err, room) => {
//         if (err) {
//           res.json(err);
//         }
//         res.redirect('/room');
//       });
//     });
//   }
// };

controller.add = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect("/room/new");
    } else {
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      //     // Database Add
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query("INSERT INTO room set ?", [data], (err, roomAdd) => {
          if (err) {
            res.json(err);
          }
          res.redirect("/room");
        });
      });
      //
    }
  }
};

controller.del = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const { id } = req.params;
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM room WHERE id=?", [id], (err, room) => {
        if (err) {
          res.json(err);
        }
        res.render("./room/roomAdminDel", {
          data: room,
          session: req.session,
        });
      });
    });
  }
};

controller.confirm = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    
    const { id } = req.params;
    req.getConnection((err, conn) => {
      conn.query("DELETE FROM room WHERE id=?", [id], (err, room) => {
        if (err) {
          const errorss = {errors:[{value: '', msg:'ไม่สามารถลบข้อมูลนี้ได้',param:'',location:''}]};
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลสำเร็จ";
        
        }
        res.redirect("/room/");
      });
    });
  }
};


controller.edit = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const { id } = req.params;
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM room WHERE id=?", [id], (err, room) => {
        res.render("./room/roomAdminEdit", {
          session: req.session,
          data: room,
        });
      });
    });
  }
};

// controller.update = (req, res) => {
//   if (typeof req.session.userid == "undefined") {
//     res.redirect("/");
//   } else {
//     const { id } = req.params;
//     const data = req.body;
//     req.getConnection((err, conn) => {
//       conn.query("update room set ? where id=?", [data, id], (err, room) => {
//         if (err) {
//           res.json(err);
//         }
//         res.redirect("/room");
//       });
//     });
//   }
// };

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
      res.redirect('/');
  } else {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          req.session.errors = errors;
          req.session.success = false;
          res.redirect(req.get('referer'))
      } else {
          req.session.success = true;
          req.session.topic = "อัพเดทข้อมูลสำเร็จ";
      //     // Database Add
          const { id } = req.params;
          const data = req.body;
          req.getConnection((err, conn) => {
              conn.query('UPDATE room SET? WHERE id=?', [data, id], (err, roomUpdate) => {
                  if (err) {
                      res.json(err);
                  }
                  res.redirect('/room');
              });
          });
          // 
      }
  };
};

module.exports = controller;
