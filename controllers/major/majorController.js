const {
  validationResult
} = require("express-validator");

const controller = {};

controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT major.id,major.name as cv FROM major', (err, major) => {
        if (err) {
          res.json(err);
        }
        res.render('./major/majorList', {
          data: major,
          session: req.session
        });
      });
    });
  };
}

controller.new = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const data = null;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department',(err,department)=>{
        conn.query('SELECT * FROM major', (err, major) => {
          res.render('./major/majorNew', {
            data1: major, data2: department,
            session: req.session
          });
        });
      })
    });
  };
}

// save
controller.save = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect('/major/new')
    } else {
      req.session.success = true;
      req.session.topic = 'เพิ่มข้อมูลสำเร็จ'
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query('INSERT INTO major set ?', [data], (err, major) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/major');
        });
      });
    }
  };
}

// delete
controller.delete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errorss = {
      errors: [{
        value: '',
        msg: 'ไม่สามารถลบข้อมูลนี้ได้',
        param: '',
        location: ''
      }]
    };
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('delete from major where id=?', [id], (err, major) => {
        if (err) {
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = 'ลบข้อมูลสำเร็จ'
        }
        res.redirect('/major');
      });
    });
  };
}


controller.edit = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department',(err,department)=>{
        conn.query('SELECT * FROM major WHERE id = ?', [id], (err, major) => {
          res.render('./major/majorUpdate', {
            data1: major,data2:department,
            session: req.session
          });
        });
      })
    });
  };
}

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect(req.get('referer'))
    } else {
      req.session.success = true;
      req.session.topic = 'แก้ไขข้อมูลสำเร็จ'
      const {
        id
      } = req.params;
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query('update major set ? where id=?', [data, id], (err, major) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/major');
        });
      });
    };
  }
}

controller.checkdelete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department',(err,department)=>{
        conn.query('SELECT * FROM major WHERE id=?', [id], (err, major) => {
          res.render('./major/majorDelete', {
            data1: major, data2: department,
            session: req.session
          });
        });
      })
    });
  };
}

module.exports = controller;