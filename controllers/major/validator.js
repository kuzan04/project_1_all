const {check} = require('express-validator');

exports.Check = [check('name','กรุณาป้อนโปรแกรมวิชา').not().isEmpty(),check('department_id','กรุณาเลือกคณะ').isNumeric()];
