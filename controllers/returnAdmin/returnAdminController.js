const controller = {};
const {
  validationResult
} = require('express-validator');

controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT b.id, p.name as pname, d.name as dname, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as borrow_date, DATE_FORMAT(b._return, "%Y/%m/%d") as _return FROM borrow as b JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id  WHERE b.id NOT IN (SELECT r.borrow_id FROM _return as r)', (err, borrows) => {
        conn.query('SELECT r.id, p.name as pname, d.name as dname, DATE_FORMAT(r.get_date,"%Y/%m/%d") as get_date, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as bdate, DATE_FORMAT(b._return, "%Y/%m/%d") as breturn FROM _return as r JOIN borrow as b ON r.borrow_id = b.id JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id;', (err, returns) => {
          if (err) {
            res.json(err);
          }
          res.render('./returnAdmin/returnAdminList', {
            session: req.session,
            data: returns, data1: borrows
          });
        })
      });
    });
  }
}

controller.save = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const data = req.body;
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect(req.get('referer'));
    } else {
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลเสร็จ";
      req.getConnection((err, conn) => {
        conn.query('insert into _return set ?;', [data], (err, fkList) => {
          if (err) {
            res.json(err);
          } else {
            res.redirect('/returnAdmin');
          }
        });
      });
    }
  }
}

controller.del = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT r.id, DATE_FORMAT(b._return, "%Y/%m/%d") as bdate, p.name as pname, d.name as dname, DATE_FORMAT(r.get_date,"%Y/%m/%d") as rdate, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as b_date FROM borrow as b JOIN _return as r ON r.borrow_id = b.id JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id WHERE r.id = ?', [id], (err, r) => {
        if (err) {
          console.log(id);
          res.json(err);
        }
        res.render('./returnAdmin/returnAdminDel', {
          session: req.session,
          data: r
        });
      })
    })
  }
}

controller.confirm = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const {
      id
    } = req.params;
    const errorss = {
      errors: [{
        value: '',
        msg: 'ลบข้อมูลนี้ไม่ได้ ',
        param: '',
        location: ''
      }]
    }
    req.getConnection((err, conn) => {
      conn.query('DELETE FROM _return WHERE id = ?', [id], (err, fkList) => {
        if (err) {
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        }
        res.redirect('/returnAdmin');
      });
    });
  }
}

controller.edit = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT b.id as bid,r.id as rid, DATE_FORMAT(b._return,"%Y/%m/%d") as rdate, DATE_FORMAT(r.get_date,"%Y/%m/%d") as get_date, p.name as pname, d.name as dname, DATE_FORMAT(b.borrow_date, "%Y/%m%/%d") as bdate FROM _return as r JOIN borrow as b ON r.borrow_id = b.id JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id HAVING rid = ?', [id], (err, fkList2) => {
        if (err) {
          res.json(err);
        }
        res.render('./returnAdmin/returnAdminEdit', {
          data: fkList2,
          session: req.session
        });
      });
    });
  }
}

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    // const errors = validationResult(req);
    const {
      id
    } = req.params;
    const errors = validationResult(req);
    const data = req.body;
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      // req.getConnection((err, conn) => {
      //   conn.query('SELECT * FROM return_ WHERE id = ?', [id], (err, fkList) => {
      //     res.render('returnAdmin/returnAdminEdit', {
      //       session: req.session,
      //       data: fkList[0]
      //     });
      //   });
      // });
      res.redirect(req.get('referer'));
    } else {
      req.session.success = true;
      req.session.topic = "แก้ไขข้อมูลสำเร็จ";
      req.getConnection((err, conn) => {
        conn.query('UPDATE _return SET ? WHERE id = ?', [data, id], (err, fkList) => {
          req.getConnection((err, conn) => {
            if (err) {
              res.json(err);
            }
            res.redirect('/returnAdmin');
          });
        });
      });
    }
  }
}

controller.add = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    req.getConnection((err, conn) => {
      const {id} = req.params;
      conn.query('select b.id , DATE_FORMAT(b._return,"%Y/%m/%d") as _return, DATE_FORMAT(b.borrow_date, "%Y/%m/%d") as borrow_date, p.name as pname, d.name as dname from borrow as b JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id WHERE b.id NOT IN (SELECT r.borrow_id FROM _return as r) HAVING b.id = ?',[id], (err, b) => {
        conn.query('select b.id as bid, r.id as rid, get_date from _return as r JOIN borrow as b ON r.borrow_id = b.id', (err, r) => {
          conn.query('select DATE_FORMAT(CURDATE(), "%Y/%m/%d") as CUR', (err, cur) => {
            res.render('./returnAdmin/returnAdminNew', {
              data: b,
              data1: r,
              data2: cur,
              session: req.session
            });
          })
        })
      });
    });
  };
}

module.exports = controller;