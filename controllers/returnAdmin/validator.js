const { check } = require('express-validator');

exports.addValidator = [
    check('borrow_id',"กรุณาเลือกรายการยืม").isNumeric()];  
    // ,
    // check('get_date',"กรุณาป้อนกำหนดวันคืน").not().isEmpty()
exports.updateValidator = [check('_return','กรุณาเลือกรายการยืม').not().isEmpty(),check('get_date','กรุณาเลือกวันที่ได้รับคืน').not().isEmpty()];
