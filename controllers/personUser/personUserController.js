const controller ={};
controller.list = (req,res) => {
  if(typeof req.session.userid == 'undefined'){
    res.redirect('/');
  }else{
    const data1 = req.session.userid;
    req.getConnection((err,conn) =>{
      conn.query('SELECT u.id as uid, p.id as pid, p.name as pname, p.studentcode as code, p.mobile as mobile, m.name as mname, dp.name as dpname, g.name as gname, u.username as user, u.password as pass FROM person as p JOIN major as m ON p.major_id = m.id JOIN department as dp ON m.department_id = dp.id JOIN _group as g ON p.group_id = g.id JOIN user as u ON u.person_id = p.id;',(err,showperson)=>{
        if(err){
          res.json(err);
        }
        res.render('./personUser/personUser',{
          data:showperson,data1,session:req.session
        });
        // console.log(data1)
      });
    });
  };
}


module.exports = controller;
