const { check } = require("express-validator");

exports.add = [check("name", "กรุณาป้อนประเภท!").not().isEmpty(),check("detail", "กรุณาป้อนรายละเอียด!").not().isEmpty()];
exports.update = [check("name", "กรุณาป้อนประเภท!").not().isEmpty(),check("detail", "กรุณาป้อนรายละเอียด!").not().isEmpty()];
