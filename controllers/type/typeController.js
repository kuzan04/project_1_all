const controller = {};
const {
  validationResult
} = require("express-validator");

controller.list = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM type", (err, type) => {
        if (err) {
          res.json(err);
        }
        res.render("./type/typeAdminList", {
          data: type,
          session: req.session,
        });
      });
    });
  }
};

controller.new = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const data = null;
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM type", (err, type) => {
        res.render("./type/typeAdminAdd", {
          data: type,
          session: req.session,
        });
      });
    });
  }
};

// controller.add = (req, res) => {
//   if (typeof req.session.userid == 'undefined') {
//     res.redirect('/');
//   } else {
//     const data = req.body;
//     req.getConnection((err, conn) => {
//       conn.query('INSERT INTO type set ?', [data], (err, type) => {
//         if (err) {
//           res.json(err);
//         }
//         res.redirect('/type');
//       });
//     });
//   }
// };

controller.add = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect("/type/new");
    } else {
      req.session.success = true;
      req.session.topic = "เพิ่มข้อมูลสำเร็จ";
      //     // Database Add
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query("INSERT INTO type set ?", [data], (err, typeAdd) => {
          if (err) {
            res.json(err);
          }
          res.redirect("/type");
        });
      });
      //
    }
  }
};

controller.del = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM type WHERE id=?", [id], (err, type) => {
        if (err) {
          res.json(err);
        }
        res.render("./type/typeAdminDel", {
          data: type,
          session: req.session,
        });
      });
    });
  }
};

controller.confirm = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query("DELETE FROM type WHERE id=?", [id], (err, type) => {
        if (err) {
          const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] };
          req.session.errors = errorss;
          req.session.success = false;
        } else {
          req.session.success = true;
          req.session.topic = "ลบข้อมูลสำเร็จ";

        }
        res.redirect("/type");
      });
    });

  }
};


controller.edit = (req, res) => {
  if (typeof req.session.userid == "undefined") {
    res.redirect("/");
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query("SELECT *FROM type WHERE id=?", [id], (err, type) => {
        res.render("./type/typeAdminEdit", {
          session: req.session,
          data: type
        });
      });
    });
  }
};

// controller.update = (req, res) => {
//   if (typeof req.session.userid == "undefined") {
//     res.redirect("/");
//   } else {
//     const { id } = req.params;
//     const data = req.body;
//     req.getConnection((err, conn) => {
//       conn.query("update type set ? where id=?", [data, id], (err, type) => {
//         if (err) {
//           res.json(err);
//         }
//         res.redirect("/type");
//       });
//     });
//   }
// };

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      req.session.errors = errors;
      req.session.success = false;
      res.redirect(req.get('referer'))
    } else {
      req.session.success = true;
      req.session.topic = "อัพเดทข้อมูลสำเร็จ";
      //     // Database Add
      const {
        id
      } = req.params;
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query('UPDATE type set? WHERE id=?', [data, id], (err, typeUpdate) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/type');
        });
      });
      // 
    }
  };
};

module.exports = controller;