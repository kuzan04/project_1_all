const controller = {};
controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    req.getConnection((err, conn) => {
      conn.query('select u.id as uid, r.id ,d.name as dn,t.name as tn,ro.name as rn , DATE_FORMAT(b.borrow_date,"%Y/%m/%d") as br,date_format(b._return,"%Y/%m/%d") as re ,DATE_FORMAT(r.get_date,"%Y/%m/%d") as _get  from user as u join person as p on u.person_id=p.id join borrow as b on b.person_id=p.id join device as d on b.device_id=d.id join _return as r on r.borrow_id=b.id join type as t on d.type_id=t.id join room as ro on d.room_id=ro.id', (err, returns) => {
        if (err) {
          res.json(err)
        }
        res.render('./returnUser/returnUser', {
          data: returns,
          session: req.session,
          data1: req.session.userid
        })
      })
    })
  }
}

controller.detail = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/')
  } else {
    const {id} = req.params;
    const data1 = req.session.userid;
    req.getConnection((err, conn) => {
      conn.query('select u.id as uid, r.id as rid,d.name as dn,t.name as tn,ra.name as rn , DATE_FORMAT(b.borrow_date,"%Y/%m/%d") as br,date_format(b._return,"%Y/%m/%d") as re ,DATE_FORMAT(r.get_date,"%Y/%m/%d") as _get,p.name as pname,p.studentcode as ps,p.mobile as pm,m.name as mname,dp.name as dpname,g.name as gname,d.code as dc from borrow as b JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id JOIN _return as r ON r.borrow_id = b.id JOIN user as u ON u.person_id = p.id join room as ra on d.room_id = ra.id join type as t on d.type_id = t.id join major as m on p.major_id=m.id join _group as g on p.group_id=g.id join department as dp on m.department_id=dp.id having rid=?',[id],(err, returns) => {
        if (err) {
          res.json(err)
        }
        res.render('./returnUser/returnDetail', {
          data: returns,
          session: req.session,
          data1: req.session.userid
        })
      })
    })
  }
}
module.exports = controller;
