const controller = {};

controller.list = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('SELECT d.name AS dname,d.code AS dcode,t.name AS tname,r.name AS rname,d.id AS did,DATE_FORMAT(B.bdate,"%Y/%m/%d") AS bdate,DATE_FORMAT(B.rdate,"%Y/%m/%d") AS rtdate,B.bid,B.bodid,B.rtboid,B.rtid FROM device AS d JOIN type AS t ON t.id = d.type_id JOIN room AS r ON r.id = d.room_id LEFT JOIN (SELECT bo.device_id AS bodid, bo.id AS bid,bo.borrow_date AS bdate,re.id AS rtid,re.get_date AS rdate,re.borrow_id AS rtboid FROM borrow AS bo LEFT JOIN _return AS re ON re.borrow_id = bo.id WHERE bo.id = (SELECT MAX(bo2.id) FROM borrow AS bo2 WHERE bo2.device_id = bo.device_id)) AS B ON B.bodid = d.id;', (err, deviceUserList) => {
                if (err) {
                    res.json(err);
                }
                res.render('./deviceUser/deviceUserList', {
                    data: deviceUserList, session: req.session
                });
            });
        });
    };
};

controller.detail = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        const { id } = req.params;
        req.getConnection((err, conn) => {
            conn.query('SELECT d.name AS dname,d.code AS dcode,t.name AS tname,r.name AS rname,t.detail AS detail,d.id AS did,DATE_FORMAT(B.bdate,"%Y/%m/%d") AS bdate,DATE_FORMAT(B.rdate,"%Y/%m/%d") AS rtdate,B.bid,B.bodid,B.rtboid,B.rtid,DATE_FORMAT(B.bre,"%Y/%m/%d") AS bre FROM device AS d JOIN type AS t ON t.id = d.type_id JOIN room AS r ON r.id = d.room_id LEFT JOIN (SELECT bo.device_id AS bodid, bo.id AS bid,bo._return AS bre,bo.borrow_date AS bdate,re.id AS rtid,re.get_date AS rdate,re.borrow_id AS rtboid FROM borrow AS bo LEFT JOIN _return AS re ON re.borrow_id = bo.id WHERE bo.id = (SELECT MAX(bo2.id) FROM borrow AS bo2 WHERE bo2.device_id = bo.device_id)) AS B ON B.bodid = d.id WHERE d.id = ?', [id], (err, deviceUserDetail) => {
                if (err) {
                    res.json(err);
                }
                res.render('./deviceUser/deviceUserDetail', {
                    data: deviceUserDetail, session: req.session
                });
            });
        });
    };
};

module.exports = controller;

// SELECT
// d.name AS dname, d.code AS dcode, t.name AS tname, r.name AS rname, d.id AS did,
//     DATE_FORMAT(B.bdate, "%Y/%m/%d") AS bdate,
//         DATE_FORMAT(B.rdate, "%Y/%m/%d") AS rtdate,
//             B.bid, B.bodid, B.rtboid, B.rtid
// FROM device AS d JOIN type AS t ON t.id = d.type_id
// JOIN room AS r ON r.id = d.room_id
// LEFT JOIN
//     (SELECT bo.device_id AS bodid, bo.id AS bid, bo.borrow_date AS bdate,
//         re.id AS rtid, re.get_date AS rdate, re.borrow_id AS rtboid 
//     FROM borrow AS bo LEFT 
//     JOIN _return AS re ON re.borrow_id = bo.id 
//         WHERE bo.id =
//             (SELECT MAX(bo2.id) 
//                 FROM borrow AS bo2 
//                 WHERE bo2.device_id = bo.device_id))
// AS B ON B.bodid = d.id;