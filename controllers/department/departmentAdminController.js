const controller = {};
const {validationResult} = require('express-validator');

controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    req.getConnection((err, conn) => {
      conn.query('SELECT department.id,department.name as nl FROM department ', (err, department) => {
        if (err) {
          res.json(err);
        }
        res.render('./department/departmentAdminList', {
          data: department,
          session: req.session
        });
      });
    });
  }
};

controller.new = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const data = null;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department', (err, department) => {
        res.render('./department/departmentAdminNew', {
          data1: department,session:req.session
        });
      });
    });
  };
}


// save
controller.save = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect('/department/new');
    }else{
      req.session.success=true;
      req.session.topic='เพิ่มข้อมูลสำเร็จ';
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query('INSERT INTO department set ?', [data], (err, department) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/department');
        });
      });
    }
  };
}

// delete
controller.confirm = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    // const errors = validationResult(req);
    const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้ ', param: '', location: '' }] }
      const {
        id
      } = req.params;
      req.getConnection((err, conn) => {
        conn.query('delete from department where id=?', [id], (err, department) => {
          if(err){
            req.session.errors=errorss;
            req.session.success=false;
            // res.redirect('/department/');
          }else{
            req.session.success=true;
            req.session.topic='ลบข้อมูลสำเร็จ';
          }
          res.redirect('/department');
        });
      });
    }
  };


controller.edit = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department WHERE id = ?',[id],(err, department) => {
        res.render('./department/departmentAdminUpdate', {
          data1: department,session:req.session
        });
      });
    });
  };
}

controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
      req.session.errors=errors;
      req.session.success=false;
      res.redirect(req.get('referer'));
    }else{
      req.session.success=true;
      req.session.topic='แก้ไขข้อมูลสำเร็จ'
      const {
        id
      } = req.params;
      const data = req.body;
      req.getConnection((err, conn) => {
        conn.query('update department set ? where id=?', [data, id], (err, department) => {
          if (err) {
            res.json(err);
          }
          res.redirect('/department');
        });
      });
    }
  };
}

controller.checkdelete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM department WHERE id=?', [id], (err, department) => {
        res.render('./department/departmentAdminDelete', {
          data1: department,session:req.session
        });
      });
    });
  };
}

module.exports = controller;