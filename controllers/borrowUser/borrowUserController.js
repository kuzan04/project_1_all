const controllers = {};
controllers.list = (req, res) => {
    if (typeof req.session.userid == 'undefined') {
        res.redirect('/');
    } else {
        req.getConnection((err, conn) => {
            conn.query('select u.id as uid, b.id ,d.name as dname,t.name as tname,ro.name as roname , DATE_FORMAT(b.borrow_date,"%Y/%m/%d") as bdate ,date_format(b._return,"%Y/%m/%d") as re from user as u join person as p on u.person_id=p.id join borrow as b on b.person_id=p.id join device as d on b.device_id=d.id join type as t on d.type_id=t.id join room as ro on d.room_id=ro.id', (err, borrow) => {
                if (err) {
                    res.json(err)
                }
                res.render('./borrowUser/borrowUser', {
                    data: borrow, data1: req.session.userid,
                    session: req.session
                });
            });
        });
    };
}

controllers.detail = (req,res) =>{
    if(typeof req.session.userid == 'undefined'){
        res.redirect('/')
    }else{
        const {id} = req.params;
        req.getConnection((err,conn)=>{
            conn.query('select * from _return',(err,returns)=>{
                conn.query('select b.id as bid,p.name as pname,p.studentcode as studentcode, p.mobile as mobile, m.name as mname, dp.name as dpname, g.name as gname, d.name as dname, d.code as code,t.name as tname,ra.name as roname, DATE_FORMAT(b.borrow_date,"%Y/%m/%d") as bdate, DATE_FORMAT(b._return,"%Y/%m/%d") as breturn from borrow as b JOIN person as p ON b.person_id = p.id JOIN device as d ON b.device_id = d.id JOIN user as u ON u.person_id = p.id join room as ra on d.room_id = ra.id join type as t on d.type_id = t.id join major as m on p.major_id=m.id join _group as g on p.group_id=g.id join department as dp on m.department_id=dp.id HAVING bid =?',[id],(err,borrows)=>{
                    if(err){
                        res.json(err)
                    }  
                    res.render('./borrowUser/borrowUserDetail',{
                        data:borrows, data2:returns, session:req.session, data1:req.session.userid
                    })
                    // res.json(data2)
                })
            })
        })
    }
}

module.exports = controllers;