const controller = {};
const {validationResult} = require('express-validator');

controller.list = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    req.getConnection((err, conn) => {
      conn.query('select d.id,d.name as dname,d.code as dcode,t.name as tname,r.name as rname from device as d join room as r on r.id = d.room_id join  type as t on t.id = d.type_id;', (err, devices) => {
        if (err) {
          res.json(err);
        }
        res.render('./deviceAdmin/deviceAdminList', {
          data: devices,
          session:req.session
        });
      });
    });
  }
};

controller.new = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const data = null;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM type', (err, type) => {
        conn.query('SELECT * FROM room', (err, room) => {
          res.render('./deviceAdmin/deviceAdminAdd', {
            data1: type,
            data2: room,
            data3: data,
            session:req.session
          });
        });
      });
    });
  }
};

controller.save = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
      res.redirect('/');
  } else {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          req.session.errors = errors;
          req.session.success = false;
          res.redirect('/deviceAdmin/new')
      } else {
        req.session.success=true;
        req.session.topic="เพิ่มข้อมูลสำเร็จ";
          const data=req.body;
          req.getConnection((err, conn) => {
              conn.query('INSERT INTO device set ?', [data], (err, deviceAdminList) => {
                if (err) {
                  res.json(err);
                }
                res.redirect('/deviceAdmin');
              });
          });
        }
    }
};

controller.edit = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM type', (err, type) => {
        conn.query('SELECT * FROM room', (err, room) => {
          conn.query('SELECT * FROM device WHERE id=?', [id], (err, device) => {
            res.render('./deviceAdmin/deviceAdminUpdate', {
              data1: type,
              data2: room,
              data3: device,
              session:req.session
            });
          });
        });
      });
    });
  }
};


controller.update = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
      res.redirect('/');
  } else {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          req.session.errors = errors;
          req.session.success = false;
          res.redirect('/deviceAdmin/update/')
      } else {
        req.session.success=true;
        req.session.topic="แก้ไขข้อมูลสำเร็จ";
        const {
          id
        } = req.params;
        const data=req.body;
    req.getConnection((err, conn) => {
      conn.query('update device set ? where id=?', [data, id], (err, devices) => {
        if (err) {
          res.json(err);
        }
        res.redirect('/deviceAdmin');
      });
    });

  }
}
};

controller.delete = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
      res.redirect('/');
  } else {
      const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
        const {
          id
        } = req.params;
        const data=req.body;
    req.getConnection((err, conn) => {
      conn.query('delete from device where id=?', [id], (err, device) => {
            if (err) {
                const errorss = { errors: [{ value: '', msg: 'ไม่สามารถลบข้อมูลนี้ได้', param: '', location: '' }] }
                req.session.errors = errorss;
                req.session.success = false;
            } else {
                req.session.success = true;
                req.session.topic = "ลบข้อมูลเสร็จแล้ว";
        }
        res.redirect('/deviceAdmin');
      });
    });
  }
};


controller.checkdel = (req, res) => {
  if (typeof req.session.userid == 'undefined') {
    res.redirect('/');
  } else {
    const {
      id
    } = req.params;
    req.getConnection((err, conn) => {
      conn.query('SELECT * FROM type', (err, type) => {
        conn.query('SELECT * FROM room', (err, room) => {
          conn.query('SELECT * FROM device WHERE id=?', [id], (err, device) => {
            res.render('./deviceAdmin/deviceAdminDel', {
              data1: type,
              data2: room,
              data3: device,
              session:req.session
            });
          });
        });
      });
    });
  }
};


module.exports = controller;
