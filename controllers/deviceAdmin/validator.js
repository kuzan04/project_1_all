const {check} = require('express-validator')

exports.deviceAdmin = [check('name','กรุณาป้อนข้อมูลชื่ออุปกรณ์!').not().isEmpty(),check('code','กรุณาป้อนข้อมูลรหัสพัสดุ!').not().isEmpty(),check('type_id','กรุณาป้อนข้อมูลประเภท').isNumeric(),check('room_id','กรุณาป้อนข้อมูลห้อง').isNumeric()]
