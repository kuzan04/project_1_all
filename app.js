const express = require('express');
const body = require('body-parser');
const cookie = require('cookie-parser');
const session = require('express-session');
const connection = require('express-myconnection');
const mysql = require('mysql');

const app = express();

const { check } = require('express-validator');
const { validationResult } = require('express-validator');

//setting
app.set('view engine','ejs');
app.set('views','views')
app.use(express.static(__dirname + '/public'));
//mldelware
app.use(body.urlencoded({extended:true}));
app.use(cookie());
app.use(session({secret:'Passw0rd',resave:true,saveUninitialized:true}));
app.use(connection(mysql,{
        host:'localhost',
        user:'root',
        password:'Passw0rd',
        port:3306,
        database:'borrowing'
},'single'));

//application
//login
app.get('/',function(req,res){
        res.render('login',{session:req.session});
})

app.post('/',[check('username','กรุณากรอก Username ใหม่!').not().isEmpty(),check('password','กรุณากรอก Passowrd ใหม่!').not().isEmpty()],function(req,res){
        const errors = validationResult(req);
        if(!errors.isEmpty()){
                req.session.errors = errors;
                req.session.success = false;
                res.render('login',{session:req.session});
        }else{
                const username = req.body.username;
                const password = req.body.password;
                req.getConnection((err,conn)=>{
                        conn.query('SELECT * FROM user WHERE username = ? and password = ?',[username,password],(err,data)=>{
                                if(err){
                                        res.json(err)
                                }else{
                                        if(data.length>0){
                                                req.session.userid=data[0].id;
                                                if(data[0].admin == 1){
                                                        res.redirect('/mainAdmin/');
                                                        // res.send(data);
                                                        // res.render('./user/userAdmin')
                                                }else{
                                                        res.redirect('/main/');
                                                        // res.send(data);
                                                        // res.render('./user/user')
                                                }
                                        }else{
                                                res.redirect('/');
                                        }
                                }
                        })
                })
        }
})

app.get('/logout',function(req,res){
        req.session.destroy(function(err){
                res.redirect('/');
        })
})

//list othder
const mainAdminRoute = require('./routes/mainAdmin');
app.use('/',mainAdminRoute);

const mainRoute = require('./routes/mainUser');
app.use('/',mainRoute);

const userRoute = require('./routes/user');
app.use('/',userRoute);

const borrowAdminRoute = require('./routes/borrowAdminRoute');
app.use('/',borrowAdminRoute);

const borrowRoute = require('./routes/borrowUserRoute');
app.use('/',borrowRoute);

const departmentRoute = require('./routes/departmentAdminRoute');
app.use('/',departmentRoute);

const deviceRoute = require('./routes/deviceUserRoute');
app.use('/',deviceRoute);

const deviceAdminRoute = require('./routes/deviceAdminRoute');
app.use('/',deviceAdminRoute);

const groupAdminRoute = require('./routes/groupRoute');
app.use('/',groupAdminRoute);

const majorAdminRoute = require('./routes/majorRoute');
app.use('/',majorAdminRoute);

const personRoute = require('./routes/personUserRoute');
app.use('/',personRoute);

const personAdminRoute = require('./routes/personAdminRoute');
app.use('/',personAdminRoute);

const returnRoute = require('./routes/returnUserRoute');
app.use('/',returnRoute);

const returnAdminRoute = require('./routes/returnAdminRoute');
app.use('/',returnAdminRoute);

const roomRoute = require('./routes/roomAdminRoute');
app.use('/',roomRoute);

const typeRoute = require('./routes/typeAdminRoute');
app.use('/',typeRoute);

app.listen(5000);
