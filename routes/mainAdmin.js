const express = require('express');
const router = express.Router();

const mainAdminController = require('../controllers/mainAdmin/mainAdminController')

router.get('/mainAdmin',mainAdminController.list);

module.exports = router;