const express = require('express');
const router = express.Router();
const showController = require('../controllers/personUser/personUserController');

router.get('/person',showController.list);

module.exports = router;
