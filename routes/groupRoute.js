const { group } = require('console');
const express = require('express');
const router = express.Router();

const groupController = require('../controllers/group/groupController');
const groupValidator = require('../controllers/group/groupValidator');

router.get('/group', groupController.list);
router.get('/group/new',groupController.new);
router.post('/group/add',groupValidator.add,groupController.add);
// router.post('/group/add',groupController.add)
router.get('/group/del/:id', groupController.del);
router.get('/group/confirm/:id', groupController.confirm);
router.get('/group/edit/:id', groupController.edit);
router.post('/group/update/:id',groupValidator.update, groupController.update);
// router.post('/group/update/:id',groupController.update);

module.exports = router;