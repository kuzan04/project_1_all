const express = require('express');
const router = express.Router();

const deviceUserController = require('../controllers/deviceUser/deviceUserController');

router.get('/device', deviceUserController.list);
router.get('/device/detail/:id', deviceUserController.detail);   

module.exports = router;