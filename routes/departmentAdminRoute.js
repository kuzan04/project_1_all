const express = require('express');
const router = express.Router();
const departmentAdminController = require('../controllers/department/departmentAdminController');
const validator = require('../controllers/department/validator');

router.get('/department',departmentAdminController.list);
router.get('/department/new',departmentAdminController.new);
router.post('/department/add',validator.Check,departmentAdminController.save);
router.get('/department/confirm/:id',departmentAdminController.confirm);
router.get('/department/update/:id',departmentAdminController.edit);
router.post('/department/update/:id',validator.Check,departmentAdminController.update);
router.get('/department/checkdelete/:id',departmentAdminController.checkdelete);

module.exports = router;
