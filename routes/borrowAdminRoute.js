const express = require('express');
const router = express.Router();

const borrowController = require('../controllers/borrowAdmin/borrowAdminController');
 const validator = require('../controllers/borrowAdmin/validator');

router.get('/borrowAdmin',borrowController.list);
router.get('/borrowAdmin/new',borrowController.new);
 router.post('/borrowAdmin/add',validator.Check,borrowController.save);
//router.post('/borrowAdmin/add',borrowController.save);
router.get('/borrowAdmin/check/:id',borrowController.confirm);
router.get('/borrowAdmin/delete/:id',borrowController.delete);
router.get('/borrowAdmin/update/:id',borrowController.edit);
 router.post('/borrowAdmin/update/:id',validator.Check,borrowController.update);
//router.post('/borrowAdmin/update/:id',borrowController.update);
router.get('/borrowAdmin/detail/:id',borrowController.detail);

module.exports = router;
