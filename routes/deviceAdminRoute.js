const express = require('express');
const router = express.Router();
const deviceController = require('../controllers/deviceAdmin/deviceAdminController');

const validator = require('../controllers/deviceAdmin/validator');

router.get('/deviceAdmin',deviceController.list);
router.get('/deviceAdmin/new',deviceController.new);
router.post('/deviceAdmin/add',validator.deviceAdmin,deviceController.save);
router.get('/deviceAdmin/edit/:id',deviceController.edit);
router.post('/deviceAdmin/update/:id',validator.deviceAdmin,deviceController.update);
router.get('/deviceAdmin/checkdel/:id',deviceController.checkdel);
router.get('/deviceAdmin/delete/:id',deviceController.delete);

module.exports = router;
