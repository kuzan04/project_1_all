const express=require('express');
const router = express.Router();

const userAdminController = require('../controllers/user/userAdminController');
// const userController = require('../controllers/user/userController');
const validator = require('../controllers/user/validator');

router.get('/user/',validator.Check,userAdminController.list);
router.get('/user/new',userAdminController.new);
router.post('/user/add',validator.Check,userAdminController.save);
router.get('/user/checkdel/:id',userAdminController.check);
router.get('/user/delete/:id',userAdminController.delete);
router.get('/user/update/:id',userAdminController.edit);
router.post('/user/update/:id',validator.Check,userAdminController.update);



// router.get('/user/',userController.list);

module.exports = router;