const express = require('express');
const router = express.Router();
const returnController = require('../controllers/returnUser/returnUserController');

router.get('/return',returnController.list);
router.get('/return/detail/:id',returnController.detail)

module.exports = router;
