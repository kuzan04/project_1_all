const express = require('express');
const router = express.Router();

const mainController = require('../controllers/mainUser/mainController');

router.get('/main',mainController.list);

module.exports = router;