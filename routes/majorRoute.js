const express = require('express');
const router = express.Router();
const majorAdminController = require('../controllers/major/majorController');
const validator = require('../controllers/major/validator');

router.get('/major',majorAdminController.list);
router.get('/major/new',majorAdminController.new);
router.post('/major/add',validator.Check,majorAdminController.save);
router.get('/major/delete/:id',majorAdminController.delete);
router.get('/major/update/:id',majorAdminController.edit);
router.post('/major/update/:id',validator.Check,majorAdminController.update);
router.get('/major/checkdelete/:id',majorAdminController.checkdelete);

module.exports = router;
