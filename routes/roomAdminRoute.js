
const { room } = require('console');
const express = require('express');
const router = express.Router();

const roomController = require('../controllers/room/roomController');
const roomValidator = require('../controllers/room/roomValidator');

router.get('/room', roomController.list);
router.get('/room/new', roomController.new);
router.post('/room/add',roomValidator.add,roomController.add);

router.get('/room/del/:id', roomController.del);
router.get('/room/confirm/:id', roomController.confirm);
router.get('/room/edit/:id', roomController.edit);
router.post('/room/update/:id',roomValidator.update, roomController.update);

module.exports = router;