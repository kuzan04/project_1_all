const express = require('express');
const router = express.Router();

const returnAdController = require('../controllers/returnAdmin/returnAdminController');
const validator = require('../controllers/returnAdmin/validator');

router.get('/returnAdmin',returnAdController.list);
router.get('/returnAdmin/new/:id',returnAdController.add);
router.post('/returnAdmin/add/',validator.addValidator,returnAdController.save);
router.get('/returnAdmin/delete/:id',returnAdController.del);
router.get('/returnAdmin/confirm/:id',validator.addValidator,returnAdController.confirm);
router.get('/returnAdmin/edit/:id',returnAdController.edit);
router.post('/returnAdmin/update/:id',validator.updateValidator,returnAdController.update);

module.exports = router;
