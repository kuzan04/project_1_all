const express = require('express');
const router = express.Router();
const prsonController = require('../controllers/personAdmin/personAdminController');
const validator = require('../controllers/personAdmin/validator');

router.get('/personAdmin',prsonController.list);
router.get('/personAdmin/new',prsonController.new);
router.post('/personAdmin/add',validator.add,prsonController.save);
router.get('/personAdmin/checkdelete/:id',prsonController.checkdelete);
router.get('/personAdmin/delete/:id',prsonController.delete);
router.get('/personAdmin/update/:id',prsonController.edit);
router.post('/personAdmin/update/:id',validator.update,prsonController.update);

module.exports = router;
