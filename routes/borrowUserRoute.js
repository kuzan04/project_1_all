const express = require('express');
const router = express.Router();

const borrowController = require('../controllers/borrowUser/borrowUserController');
// const validator = require('../controllers/validator');

router.get('/borrow/',borrowController.list);
router.get('/borrow/detail/:id',borrowController.detail);


module.exports = router;