
const { type } = require('console');
const express = require('express');
const router = express.Router();

const typeController = require('../controllers/type/typeController');
const typeValidator = require('../controllers/type/typeValidator');

router.get('/type', typeController.list);
router.get('/type/new',typeController.new);
router.post('/type/add',typeValidator.add,typeController.add);
// router.post('/group/add',groupController.add)
router.get('/type/del/:id', typeController.del);
router.get('/type/confirm/:id', typeController.confirm);
router.get('/type/edit/:id', typeController.edit);
router.post('/type/update/:id',typeValidator.update, typeController.update);
// router.post('/group/update/:id',groupController.update);
module.exports = router;